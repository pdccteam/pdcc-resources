HOWTO: Installing NViDIA CUDA Toolkit on laptop/notebook GPU-support
===================================================================
Credit goes to respective authors. 

Maintainer(s)
-------------
hadrihilmi@gmail.com

TODO list
---------
* testing install in Ubuntu 12.04 (done)
* testing install in Windows box + VS2010 (in progress)

Installing in Ubuntu 12.04
--------------------------
Follow this tutorial (http://www.r-tutor.com/gpu-computing/cuda-installation/cuda5.0-ubuntu). Although the author did install in Ubuntu 11.10, installing in Ubuntu 12.04 was tested successful. 

p/s: You will experience an unsuccessful installation on CUDA samples. 
This is due to CUDA 5 is not yet (as this readme was written on 12/12/12) supported on Ubuntu 12.04, as mention by Przemyslaw Zych in stackoverflow (http://stackoverflow.com/questions/12986701/installing-cuda-5-samples-in-ubuntu-12-10) and official release note of CUDA 5 (http://developer.download.nvidia.com/compute/cuda/5_0/rel/docs/CUDA_Toolkit_Release_Notes_And_Errata.txt).