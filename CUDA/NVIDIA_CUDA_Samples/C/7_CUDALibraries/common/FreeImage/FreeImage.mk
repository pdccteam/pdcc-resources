.PHONY: update_freeimage_files

include $(ROOTDIR)/build/config/DetectOS.mk

OS_ARCH = $(shell uname -m | sed -e "s/i386/i686/")

FREEIMAGE_FILES += include/FreeImage.h

ifeq ($(OS), win32)
ifeq ($(HOST_ARCH), i686)
FREEIMAGE_FILES += lib/Win32/FreeImage.dll lib/Win32/FreeImage.lib
else
FREEIMAGE_FILES += lib/x64/FreeImage64.dll lib/x64/FreeImage64.lib
endif
endif

ifeq ($(OS), Linux)
FREEIMAGE_FILES += lib/linux/$(OS_ARCH)/libfreeimage.a
endif

DARWIN = $(strip $(findstring DARWIN, $(OSUPPER)))

ifneq ($(DARWIN),)
FREEIMAGE_FILES += lib/darwin/libfreeimage.a
endif

update_freeimage_files:
	$(foreach freeimage_files,$(FREEIMAGE_FILES),$(CP) -rf $(SRC_CWD)/$(freeimage_files) $(DRIVELETTER)$(BINDIR);)

include $(ROOTDIR)/build/common.mk
